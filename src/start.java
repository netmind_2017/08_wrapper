
public class start {

	public static void main(String[] args) {
		
		
		//String to Boolean 
		Boolean x= Boolean.valueOf("true");
		if (x) {
			System.out.println("x is boolean");
		}
		
		//String to Integer
		int y = Integer.parseInt("100");		
		if (y == 100){
			System.out.println("y is Integer");
		}
		
		//String to Float
		Float z= Float.valueOf("0.25");
		if (z == 0.25){
			System.out.println("z is Float");
		}
		
		//Integer to String
		String w = String.valueOf(100);
		if (w.equals("100")){
			System.out.println("w is String");
		}
 		
	}

}
